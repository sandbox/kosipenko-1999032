<?php
/**
 * @file
 * Cache Controller class for nodes.
 */

/**
 * This extends the NodeController class, adding functionality
 * for loading node objects from serialized field, to reduce page load time.
 */
if (class_exists('EntityCacheNodeController')) {
  class CacheEntityCacheNodeController extends EntityCacheNodeController {

    /**
     * Load of node.
     */
    protected function attachLoad(&$nodes, $revision_id = FALSE) {
      // Create an array of nodes for each content type and pass this to the
      // object type specific callback.
      $typed_nodes = $nodes_normal = $nodes_unserialized = array();
      foreach ($nodes as $id => $entity) {
        // If we have serialized node, then use it without additional
        // processing.
        if (isset($entity->serialize_node)) {
          $nodes_unserialized[$id] = unserialize($entity->serialize_node);
        }
        else {
          $nodes_normal[$id] = $entity;
        }
        $typed_nodes[$entity->type][$id] = $entity;
      }

      // Besides the list of nodes, pass one additional argument to
      // hook_node_load(), containing a list of node types that were loaded.
      $argument = array_keys($typed_nodes);
      $this->hookLoadArguments = array($argument);

      // If we have unserialized nodes then process it as usual.
      if (!empty($nodes_normal)) {
        // Call object type specific callbacks on each typed array of nodes.
        foreach ($typed_nodes as $node_type => $nodes_of_type) {
          if (node_hook($node_type, 'load')) {
            $function = node_type_get_base($node_type) . '_load';
            $function($nodes_of_type);
          }
        }

        // Attach fields.
        if ($this->entityInfo['fieldable']) {
          if ($revision_id) {
            field_attach_load_revision($this->entityType, $nodes_normal);
          }
          else {
            field_attach_load($this->entityType, $nodes_normal);
          }
        }
      }

      $nodes = $nodes_unserialized + $nodes_normal;
      // Call hook_entity_load().
      foreach (module_implements('entity_load') as $module) {
        $function = $module . '_entity_load';
        $function($nodes, $this->entityType);
      }
      // Call hook_TYPE_load(). The first argument for hook_TYPE_load() are
      // always the queried entities, followed by additional arguments set in
      // $this->hookLoadArguments.
      $args = array_merge(array($nodes), $this->hookLoadArguments);
      foreach (module_implements($this->entityInfo['load hook']) as $module) {
        call_user_func_array($module . '_' . $this->entityInfo['load hook'], $args);
      }
      // Finally iterate over nodes and serialize it if node wasn't
      // serialized before.
      foreach ($nodes_normal as $node) {
        cache_node_object_update($node);
      }
    }
  }
}
